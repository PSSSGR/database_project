// insertArtist.java

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
public class insertArtist{
  public static void main(String args[]){
    Connection connection = null;
    try{

      // Step 1: Connect to the database server.
      String host = "cslab-db.cs.wichita.edu";
      int port = 3306;
      String database = "dbuser22_database";
      String user = "dbuser22";
      String password = "LQybrdgRpuvk";
      String url = String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, database, user, password);
    	System.out.println("Testing connection to database: " + database);
    	try {
    	    try (Connection con = DriverManager.getConnection(url)){
    		System.out.println("Successfully connected to database: " + database);
    	    }
    	} catch (Exception e){
    	    e.printStackTrace();
    	}
      connection = DriverManager.getConnection(url);

    //step 2: input
	Statement stmt = connection.createStatement();

	System.out.println("Inserting into Artist table: ");
  System.out.println("Please type the name of the artist you would like to insert: ");
  Scanner scanner = new Scanner(System.in);
  String nameTemp = scanner.nextLine();


    //step 3: insertion
    String cmd = "INSERT into Artist (artist_name) VALUES ('" + nameTemp + "')";
	  ResultSet rs = stmt.executeQuery(cmd);
    try{
		stmt.executeUpdate(cmd);
		rs = stmt.executeQuery(cmd);
		System.out.println("Insert successful");
	   } catch(Exception e){
		System.out.println("Insert failed, try again");
	   }
      rs.close();
      System.out.format("%n");
    }

    //leave

    catch(SQLException e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if(connection != null)
        connection.close();
      }
      catch(SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
}
