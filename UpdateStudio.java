// File: UpdateAlbum.java
//
// An example to show how to use the executeUpdate function of
// the Statement class. The Year of an Album record will
// be changed. The ID of the Album is 1.
//
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
public class UpdateStudioAddress{
  public static void main(String[] args){
    Connection connection = null;
    try{
      // Step 1: Connect to the database server.
      String host = "cslab-db.cs.wichita.edu";
      int port = 3306;
      String database = "dbuser22_database";
      String user = "dbuser22";

      String password = "LQybrdgRpuvk";
      String url = String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, database, user, password);
    	// System.out.println("DEBUG: db_connection string = " + db_connection);
    //  String url = "jbdc:mariadb://cslab-db.cs.wichita.edu:3306/dbuser22_database/user=dbuser22&password=LQybrdgRpuvk";
    	System.out.println("Testing connection to database: " + database);
    	try {
    	    try (Connection con = DriverManager.getConnection(url)){
    		System.out.println("Successfully connected to database: " + database);
    	    }
    	} catch (Exception e){
    	    e.printStackTrace();
    	}
      connection = DriverManager.getConnection(url);
      // Step 2: Show the MajorId of student number 1.
      Statement stmt = connection.createStatement();
      Scanner keyboard = new Scanner(System.in);
      int studioId;
      String address;
      System.out.println("Enter the Studio ID(1-10) you wish to update the address of : ");
      studioId = keyboard.nextInt();
      keyboard.nextLine();
      System.out.println("Enter the Address you want to update : ");
      address = keyboard.nextLine();
      String qry = "select address from Studio where studio_id = " + studioId;
      System.out.printf("Here is the original address for Studio number %d:",studioId);
      ResultSet rs = stmt.executeQuery(qry);
      if (rs.next()){
        String Address = rs.getString("address");
        System.out.format("%3s%n", Address);
      }
      // Step 3: change the MajorId to 20.
    //  String cmd = "update Studio set address = "+ address + "where album_id = "+studioId;
      String cmd = String.format("update Studio set address = '%s' where studio_id = %d", address, studioId);
      stmt.executeUpdate(cmd);
      // Step 4: Show the changed MajorId of student number 1.
      System.out.printf("Here is the updated address for Studio number %d: ",studioId);
      rs = stmt.executeQuery(qry);
      if (rs.next()){
        String Address = rs.getString("address");
        System.out.format("%3s%n", Address);
      }
      rs.close();
      System.out.format("%n");
    }
    catch(SQLException e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if(connection != null)
        connection.close();
      }
      catch(SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
}
