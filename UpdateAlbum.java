// File: UpdateAlbum.java
//
//
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
public class UpdateAlbum{
  public static void main(String[] args){
    Connection connection = null;
    try{
      // Step 1: Connect to the database server.
      String host = "cslab-db.cs.wichita.edu";
      int port = 3306;
      String database = "dbuser22_database";
      String user = "dbuser22";

      String password = "LQybrdgRpuvk";
      String url = String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, database, user, password);
    	// System.out.println("DEBUG: db_connection string = " + db_connection);
    //  String url = "jbdc:mariadb://cslab-db.cs.wichita.edu:3306/dbuser22_database/user=dbuser22&password=LQybrdgRpuvk";
    	System.out.println("Testing connection to database: " + database);
    	try {
    	    try (Connection con = DriverManager.getConnection(url)){
    		System.out.println("Successfully connected to database: " + database);
    	    }
    	} catch (Exception e){
    	    e.printStackTrace();
    	}
      connection = DriverManager.getConnection(url);
      // Step 2: Show the MajorId of student number 1.
      Statement stmt = connection.createStatement();
      Scanner keyboard = new Scanner(System.in);
      int albumId;
      int year;
      System.out.println("Enter the Album Id(1-9) for which you wish to update the Year: ");
      albumId = keyboard.nextInt();
      System.out.println("Enter the Year you wish to update: ");
      year = keyboard.nextInt();
      String qry =  String.format("select year from Album where album_id = %s", albumId);
    //  String qry = "select Year from Album where album_id = 1";
      System.out.printf("Here is the original Year for Album number %d:",albumId);
      ResultSet rs = stmt.executeQuery(qry);
      if (rs.next()){
        int Year = rs.getInt("Year");
        System.out.format("%3d%n", Year);
      }
      // Step 3: change the MajorId to 20.

      String cmd = "update Album set Year =" + year + " where album_id = "+albumId;
      stmt.executeUpdate(cmd);
      // Step 4: Show the changed MajorId of student number 1.
      System.out.printf("Here is the updated year for Album number %d: ",albumId);
      rs = stmt.executeQuery(qry);
      if (rs.next())
      {
        int Year = rs.getInt("Year");
        System.out.format("%3d%n", Year);
      }
      rs.close();
      System.out.format("%n");
    }
    catch(SQLException e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if(connection != null)
        connection.close();
      }
      catch(SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
}
