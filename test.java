/*
To compile and run this code:
$ javac test.java
$ java -cp "mariadb-java-client-2.4.1.jar:." test

For further information see: https://mariadb.com/kb/en/library/about-mariadb-connector-j/
 */

import java.sql.Connection;
import java.sql.DriverManager;

public class test {
    public static void main(String[] args) {	
	// database specific parameters
	String host = "cslab-db.cs.wichita.edu";
	int port = 3306;
	String database = "dbuser22_database";
	String user = "dbuser22";
	String password = "LQybrdgRpuvk";

	String db_connection = String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, database, user, password);
	// System.out.println("DEBUG: db_connection string = " + db_connection);

	System.out.println("Testing connection to database: " + database);
	try {
	    try (Connection con = DriverManager.getConnection(db_connection)){
		System.out.println("Successfully connected to database: " + database);
	    }
	} catch (Exception e){
	    e.printStackTrace();
	}
    }
}
